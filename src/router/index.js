import Vue from 'vue';
import VueRouter from 'vue-router';
import firebase from 'firebase/app';

Vue.use(VueRouter);

const routes = [
  {
    path: '/login',
    name: 'Login',
    meta: { layout: 'auth' },
    component: () => import('../views/Login.vue')
  },
  {
    path: '/signup',
    name: 'Register',
    meta: { layout: 'auth' },
    component: () => import('../views/Register.vue')
  },
  {
    path: '/recovery',
    name: 'Recovery',
    meta: { layout: 'auth' },
    component: () => import('../views/Recovery.vue')
  },
  {
    path: '/profile-edit',
    name: 'ProfileEdit',
    meta: { auth: true, layout: 'main' },
    component: () => import('../views/ProfileEdit.vue')
  },
  {
    path: '/favorites',
    name: 'Favorites',
    meta: { auth: true, layout: 'main' },
    component: () => import('../views/Favorites.vue')
  },
  {
    path: '/',
    name: 'Home',
    meta: { auth: true, layout: 'main' },
    component: () => import('../views/Home.vue')
  },
  {
    path: '/hotel/:id',
    name: 'HotelMore',
    meta: { auth: true, layout: 'main' },
    component: () => import('../views/HotelMore.vue')
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  const currentUser = firebase.auth().currentUser;
  const requireAuth = to.matched.some(record => record.meta.auth);

  if (requireAuth && !currentUser) {
    next('/login');
  } else {
    next();
  }
});

export default router;
