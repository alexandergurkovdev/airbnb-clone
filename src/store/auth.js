import firebase from 'firebase/app';

export default {
  namespaced: true,
  state: {
    loading: false,
    error: null
  },
  actions: {
    async signUp({ commit }, { firstName, lastName, email, password }) {
      try {
        commit('setLoading', true);
        const res = await firebase.auth().createUserWithEmailAndPassword(email, password);

        const user = firebase.auth().currentUser;
        await user.sendEmailVerification();

        await firebase
          .firestore()
          .collection('users')
          .doc(res.user.uid)
          .set({
            firstName,
            lastName
          });

        commit('setLoading', false);
      } catch (error) {
        commit('setLoading', false);
        commit('setError', error.message);
        throw error;
      }
    },
    async signIn({ commit }, { email, password }) {
      try {
        commit('setLoading', true);
        await firebase.auth().signInWithEmailAndPassword(email, password);
        commit('setLoading', false);
      } catch (error) {
        commit('setLoading', false);
        commit('setError', error.message);
        throw error;
      }
    },
    async recoveryPassword({ commit }, { email }) {
      try {
        commit('setLoading', true);
        await firebase.auth().sendPasswordResetEmail(email);
        commit('setLoading', false);
      } catch (error) {
        commit('setLoading', false);
        commit('setError', error.message);
        throw error;
      }
    },
    async logout() {
      await firebase.auth().signOut();
    }
  },
  mutations: {
    setLoading(state, loading) {
      state.loading = loading;
    },
    setError(state, error) {
      state.error = error;
    },
    init(state) {
      state.error = null;
      state.loading = false;
    }
  },
  getters: {
    getLoading(state) {
      return state.loading;
    },
    getError(state) {
      return state.error;
    }
  }
};
