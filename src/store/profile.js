import firebase from 'firebase/app';

export default {
  namespaced: true,
  state: {
    loading: false,
    error: null,
    user: {
      firstName: null,
      lastName: null,
      email: null,
      emailVerified: undefined
    }
  },
  actions: {
    async fetchUser({ commit }) {
      const user = firebase.auth().currentUser;

      const currentUser = await firebase
        .firestore()
        .collection('users')
        .doc(user.uid)
        .get();
      commit('setEmailVerified', user.emailVerified);
      commit('setEmail', user.email);
      commit('setCurrentUser', currentUser.data());
    },
    async editProfile({ commit }, { email, firstName, lastName, password }) {
      const user = firebase.auth().currentUser;

      try {
        commit('setLoading', true);
        if (user.email !== email) {
          await user.updateEmail(email);
        }

        await firebase
          .firestore()
          .collection('users')
          .doc(user.uid)
          .set({
            firstName: firstName,
            lastName: lastName
          });

        if (password.length > 0) {
          await user.updatePassword(password);
        }
        commit('setLoading', false);
      } catch (error) {
        commit('setLoading', false);
        commit('setError', error.message);
        throw error;
      }
    }
  },
  mutations: {
    setLoading(state, loading) {
      state.loading = loading;
    },
    setError(state, error) {
      state.error = error;
    },
    setCurrentUser(state, user) {
      state.user.firstName = user.firstName;
      state.user.lastName = user.lastName;
    },
    setEmail(state, email) {
      state.user.email = email;
    },
    setEmailVerified(state, emailVerified) {
      state.user.emailVerified = emailVerified;
    }
  },
  getters: {
    getLoading(state) {
      return state.loading;
    },
    getError(state) {
      return state.error;
    },
    getUser(state) {
      return state.user;
    }
  }
};
