import firebase from 'firebase/app';

export default {
  namespaced: true,
  state: {
    loading: false,
    error: null,
    hotels: [],
    viewHotel: null
  },
  actions: {
    async fetchHotels({ commit }) {
      try {
        commit('setLoading', true);
        const res = await firebase
          .firestore()
          .collection('hotels')
          .get();
        if (res) {
          const hotels = res.docs.map(doc => doc.data());
          commit('setHotels', hotels);
        }
        commit('setLoading', false);
      } catch (error) {
        commit('setLoading', false);
        commit('setError', error.message);
        throw error;
      }
    },
    async fetchHotel({ commit }, id) {
      try {
        commit('setLoading', true);
        const res = await firebase
          .firestore()
          .collection('hotels')
          .doc(String(id))
          .get();
        if (res) {
          commit('setHotel', res.data());
        }
        commit('setLoading', false);
      } catch (error) {
        commit('setLoading', false);
        commit('setError', error.message);
        throw error;
      }
    }
  },
  mutations: {
    setLoading(state, loading) {
      state.loading = loading;
    },
    setError(state, error) {
      state.error = error;
    },
    setHotels(state, hotels) {
      state.hotels = hotels;
    },
    setHotel(state, hotel) {
      state.viewHotel = hotel;
    }
  },
  getters: {
    getLoading(state) {
      return state.loading;
    },
    getError(state) {
      return state.error;
    },
    getHotels(state) {
      return state.hotels;
    },
    getHotel(state) {
      return state.viewHotel;
    }
  }
};
