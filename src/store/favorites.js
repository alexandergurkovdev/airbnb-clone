import firebase from 'firebase/app';

export default {
  namespaced: true,
  state: {
    loading: false,
    error: null,
    favorites: []
  },
  actions: {
    async addToFavorite({ commit, dispatch }, hotel) {
      commit('setLoading', true);

      const user = firebase.auth().currentUser;

      const res = await firebase
        .firestore()
        .collection('favorites')
        .doc(user.uid)
        .get();

      if (!res.data().favorites || res.data().favorites.length === 0) {
        await firebase
          .firestore()
          .collection('favorites')
          .doc(user.uid)
          .set({
            favorites: [hotel]
          });
      } else {
        await firebase
          .firestore()
          .collection('favorites')
          .doc(user.uid)
          .update({
            favorites: [...res.data().favorites, hotel]
          });
      }

      await dispatch('fetchFavorites');

      commit('setLoading', false);
    },
    async removeFromFavorite({ commit, dispatch }, id) {
      commit('setLoading', true);

      const user = firebase.auth().currentUser;

      const res = await firebase
        .firestore()
        .collection('favorites')
        .doc(user.uid)
        .get();

      if (res.data()) {
        const newFavorites = res.data().favorites.filter(item => item.id !== id);

        await firebase
          .firestore()
          .collection('favorites')
          .doc(user.uid)
          .update({
            favorites: newFavorites
          });
      }

      await dispatch('fetchFavorites');

      commit('setLoading', false);
    },
    async fetchFavorites({ commit }) {
      commit('setLoading', true);

      const user = firebase.auth().currentUser;

      const res = await firebase
        .firestore()
        .collection('favorites')
        .doc(user.uid)
        .get();

      if (res.data()) {
        commit('setFavorites', res.data().favorites);
      }
      commit('setLoading', false);
    }
  },
  mutations: {
    setLoading(state, loading) {
      state.loading = loading;
    },
    setError(state, error) {
      state.error = error;
    },
    setFavorites(state, favorites) {
      state.favorites = favorites;
    }
  },
  getters: {
    getLoading(state) {
      return state.loading;
    },
    getError(state) {
      return state.error;
    },
    getFavorites(state) {
      return state.favorites;
    }
  }
};
