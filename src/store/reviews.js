import firebase from 'firebase/app';

export default {
  namespaced: true,
  state: {
    loading: false,
    error: null,
    reviews: []
  },
  actions: {
    async addReview({ commit, dispatch }, { text, id, rating, user }) {
      commit('setLoading', true);
      const currentUser = firebase.auth().currentUser;

      const res = await firebase
        .firestore()
        .collection('reviews')
        .doc(String(id))
        .get();

      const newReview = {
        user: {
          ...user,
          uid: currentUser.uid
        },
        rating,
        text,
        timestamp: Date.now()
      };

      const hotel = await firebase
        .firestore()
        .collection('hotels')
        .doc(String(id))
        .get();

      if (hotel) {
        await firebase
          .firestore()
          .collection('hotels')
          .doc(String(id))
          .update({
            ...hotel.data(),
            rating: (hotel.data().rating + rating) / 2
          });
      }

      if (res.data()) {
        await firebase
          .firestore()
          .collection('reviews')
          .doc(String(id))
          .update({
            reviews: [...res.data().reviews, newReview]
          });
      } else {
        await firebase
          .firestore()
          .collection('reviews')
          .doc(String(id))
          .set({
            reviews: [newReview]
          });
      }

      await dispatch('fetchReviews', id);

      commit('setLoading', false);
    },
    async deleteReview({ commit, dispatch }, { hotelId, userId }) {
      commit('setLoading', true);
      const res = await firebase
        .firestore()
        .collection('reviews')
        .doc(String(hotelId))
        .get();

      if (res.data()) {
        const newReviews = res.data().reviews.filter(item => item.user.uid !== userId);
        await firebase
          .firestore()
          .collection('reviews')
          .doc(String(hotelId))
          .set({
            reviews: newReviews
          });
      }

      await dispatch('fetchReviews', String(hotelId));

      commit('setLoading', false);
    },
    async fetchReviews({ commit }, id) {
      commit('setLoading', true);
      await firebase
        .firestore()
        .collection('reviews')
        .doc(String(id))
        .onSnapshot(doc => {
          commit('setReviews', doc.data().reviews);
        });

      commit('setLoading', false);
    }
  },
  mutations: {
    setLoading(state, loading) {
      state.loading = loading;
    },
    setError(state, error) {
      state.error = error;
    },
    setReviews(state, reviews) {
      state.reviews = reviews;
    }
  },
  getters: {
    getLoading(state) {
      return state.loading;
    },
    getError(state) {
      return state.error;
    },
    getReviews(state) {
      return state.reviews;
    }
  }
};
