import Vue from 'vue';
import Vuex from 'vuex';
import auth from './auth';
import profile from './profile';
import hotels from './hotels';
import favorites from './favorites';
import reviews from './reviews';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: { auth, profile, hotels, favorites, reviews }
});
