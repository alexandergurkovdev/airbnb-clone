import Vue from 'vue';
import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';
import router from './router';
import store from './store';
import dateFilter from './filters/date.filter';
import { LMap, LTileLayer, LMarker, LPopup } from 'vue2-leaflet';
import ElementUI from 'element-ui';
import locale from 'element-ui/lib/locale/lang/en';
import App from './App.vue';
import 'element-ui/lib/theme-chalk/reset.css';
import 'element-ui/lib/theme-chalk/index.css';
import 'leaflet/dist/leaflet.css';

Vue.config.productionTip = false;
Vue.use(ElementUI, { locale });
Vue.filter('date', dateFilter);
Vue.component('l-map', LMap);
Vue.component('l-tile-layer', LTileLayer);
Vue.component('l-marker', LMarker);
Vue.component('l-popup', LPopup);

const firebaseConfig = {
  apiKey: 'AIzaSyCsWoHkMk2Vf8nqzZ0S2IKWWKH5ob4EJt8',
  authDomain: 'vue-code-helper.firebaseapp.com',
  databaseURL: 'https://vue-code-helper.firebaseio.com',
  projectId: 'vue-code-helper',
  storageBucket: 'vue-code-helper.appspot.com',
  messagingSenderId: '137692118217',
  appId: '1:137692118217:web:44904dfebaef8691332118'
};

firebase.initializeApp(firebaseConfig);

let app;

firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount('#app');
  }
});
